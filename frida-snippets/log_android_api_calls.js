/**
 * Log calls to Android APIs
 *
 * Based on https://github.com/iddoeldor/frida-snippets/blob/master/README.md#change-location
 * and https://gist.github.com/adamyordan/848a11b33c8b5455b07ea669e7319cdd.
 *
 * and https://github.com/microsoft/SARA/
 */
Java.perform(() => {
  let _log = function (str) {
    let AndroidLog = Java.use("android.util.Log");
    let PREFIX = "[FRIDA ANDROID API CALLS]";
    AndroidLog.v(PREFIX, str);
    console.log(`${PREFIX} ${str}`);
  };

  // Instrument target classes and methods for logging calls and returned values.
  // Instrumented APIs are : AOSP location.
  const targetsClassesMethods = {
    // Android location calls
    "android.location.Location": ["getLatitude", "getLongitude"],
    // Access to GAID
    "com.google.android.gms.ads.identifier.AdvertisingIdClient": [
      "getAdvertisingIdInfo",
    ],
  };
  Object.keys(targetsClassesMethods).forEach(function (targetClass) {
    try {
      const javaTargetClass = Java.use(targetClass);

      targetsClassesMethods[targetClass].forEach(function (targetMethod) {
        javaTargetClass[targetMethod].implementation = function () {
          const argumentsJson = JSON.stringify(arguments, null, 2);
          const returnValue = javaTargetClass[targetMethod].apply(
            this,
            arguments
          );

          _log(
            `CALLED ${targetClass}.${targetMethod}, ARGUMENTS ${argumentsJson}, RETURNED ${returnValue}`
          );

          return returnValue;
        };
      });
    } catch {
      console.error(`Unable to find matching class ${targetClass}.`);
    }
  });

  // Inspect all classes and dump all susceptible listeners (location or sensors) to instrment.
  const package_name = Java.use("android.app.Application").getProcessName();
  /* FIXME: Java.enumerateLoadedClasses({
    onMatch: function (className) {
      let isSensorEventListener = false;
      let isLocationListener = false;
      if (className.toLowerCase().startsWith(package_name)) {
        let classInstance = Java.use(className);
        let proto = classInstance.__proto__;
        // Inspect SensorEventListener
        isSensorEventListener =
          proto.hasOwnProperty("onSensorChanged") &&
          proto.hasOwnProperty("onAccuracyChanged");
        // Inspect LocationListener
        isLocationListener =
          proto.hasOwnProperty("onLocationChanged") &&
          proto.hasOwnProperty("onProviderDisabled") &&
          proto.hasOwnProperty("onProviderEnabled");

        _log(
          JSON.stringify({
            className: className,
            isSensorEventListener: isSensorEventListener,
            isLocationListener: isLocationListener,
          })
        );
      }
    },
    onComplete: function () {},
  });*/

  // Instrument all sensor listeners to log calls and values.
  let sensorManager = Java.use("android.hardware.SensorManager");
  sensorManager.registerListener.overload(
    "android.hardware.SensorEventListener",
    "android.hardware.Sensor",
    "int"
  ).implementation = function (listener, sensor, period) {
    let timestamp = +new Date() / 1000;
    _log(
      JSON.stringify({
        msgType: "listener",
        listenerHandle: listener.$handle,
        listenerClassname: listener.$className,
        sensorType: sensor.getStringType(),
        sensorIntType: sensor.getType(),
        period: period,
        timestamp: timestamp,
        target: "" + this,
      })
    );

    let className = Java.use(listener.$className);
    let methodHandle = className.onSensorChanged.handle;
    _log(
      JSON.stringify({
        msgType: "handle",
        handle: methodHandle,
        timestamp: timestamp,
        target: "" + this,
      })
    );
    className.onSensorChanged.implementation = function (sensorEvent) {
      let values = sensorEvent.values;
      let timestamp = +new Date() / 1000;
      _log(
        JSON.stringify({
          msgType: "sensorEvent",
          target: "" + this,
          values: values.value,
          timestamp: timestamp,
          className: className,
          sensorType: sensorEvent.sensor.value.getStringType(),
        })
      );

      return this.onSensorChanged(sensorEvent);
    };

    return sensorManager.registerListener.apply(this, arguments);
  };

  // Instrument battery manager to log calls and values.
  let batteryManager = Java.use("android.os.BatteryManager");
  batteryManager.class.getDeclaredMethods().forEach(function (method) {
    let name = method.getName();

    batteryManager[name].implementation = function (...args) {
      let timestamp = +new Date() / 1000;
      let value = this[name](...args);
      _log(
        JSON.stringify({
          msgType: "sensorEvent",
          target: "" + this,
          args: args,
          value: value,
          timestamp: timestamp,
          className: `android.os.BatteryManager.${name}`,
          sensorType: "battery",
        })
      );
      return value;
    };
  });

  // Telephony manager (SIM / operator / etc.)
  let telephonyManager = Java.use("android.telephony.TelephonyManager");
  telephonyManager.class.getDeclaredMethods().forEach(function (method) {
    let name = method.getName();

    telephonyManager[name].overloads.forEach(function (overload, index) {
      overload.implementation = function (...args) {
        let timestamp = +new Date() / 1000;
        let value = this[name](...args);
        _log(
          JSON.stringify({
            msgType: "TelephonyManager",
            target: "" + this,
            args: args,
            value: value,
            timestamp: timestamp,
            className: `android.telephony.TelephonyManager.${name}`,
            sensorType: "Telephony/SIM",
          })
        );
        return value;
      };
    });
  });

  // Wifi manager
  let wifiManager = Java.use("android.net.wifi.WifiManager");
  wifiManager.class.getDeclaredMethods().forEach(function (method) {
    let name = method.getName();

    wifiManager[name].overloads.forEach(function (overload, index) {
      overload.implementation = function (...args) {
        let timestamp = +new Date() / 1000;
        let value = this[name](...args);
        _log(
          JSON.stringify({
            msgType: "WifiManager",
            target: "" + this,
            args: args,
            value: value,
            timestamp: timestamp,
            className: `android.net.wifi.WifiManager.${name}`,
            sensorType: "WIFI",
          })
        );
        return value;
      };
    });
  });

  // Hook ContentResolver API (including Contacts)
  let ContentResolver = Java.use("android.content.ContentResolver");
  ContentResolver.query.overloads.forEach(function (overload, index) {
    overload.implementation = function (...args) {
      let timestamp = +new Date() / 1000;
      let uri = args[0];
      _log(
        JSON.stringify({
          msgType: "ContentResolver",
          target: "" + this,
          args: args,
          timestamp: timestamp,
          className: "android.content.ContentResolver.query",
          uri: uri,
        })
      );
      return this.query(...args);
    };
  });
});
