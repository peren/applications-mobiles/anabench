# Documentation for developers with ANABENCH

## Useful global utilities

See endpoints interactive doc for more details.
- List devices and serials of connected phones : `/devices`
- Android tool to dump system state : `/dumpsys`
- Download app : `/apk/{package}`
- Record screen phone : `/record/start` and `/record/end`

## Work with ADB (Android Debug Bridge)

- [Official landing page](https://developer.android.com/tools/adb)
- [`shell` commands](https://developer.android.com/tools/adb#shellcommands):
    * [List of `input keyevents`](https://gist.github.com/arjunv/2bbcca9a1a1c127749f8dcb6d36fb0bc) : Actions on physical or virtual buttons of the phone 



## Work on ROBOT FRAMEWORK automations

### Troubleshooting

- `Original error: Error: socket hang up` : [adb uninstall io.appium.uiautomator2.server; adb uninstall io.appium.uiautomator2.server.test](https://stackoverflow.com/a/72710183)


### Resources

- [Official landing page](https://robotframework.org/)
- [ROBOT FRAMEWORK official resources](https://docs.robotframework.org/):
    * [Robot Framework Guides](https://docs.robotframework.org/docs) : Get started with simple examples and hints
    * [Robot Framework User Guide](https://robotframework.org/robotframework/latest/RobotFrameworkUserGuide.html) : The full reference manual for the Robot Framework Core. Learn about the syntax, file types, how to extend it and much more
    * [Standard Library](https://robotframework.org/robotframework/#standard-libraries) : Interacting with the app ([BuiltIn](https://robotframework.org/robotframework/latest/libraries/BuiltIn.html), [Dialogs](https://robotframework.org/robotframework/latest/libraries/Dialogs.html), [Screenshot](https://robotframework.org/robotframework/latest/libraries/Screenshot.html)), manipulating types ([Collections](https://robotframework.org/robotframework/latest/libraries/Collections.html), [DateTime](https://robotframework.org/robotframework/latest/libraries/DateTime.html), [String](https://robotframework.org/robotframework/latest/libraries/String.html), [XML](https://robotframework.org/robotframework/latest/libraries/XML.html)), interacting with system and network ([OperatingSystem](https://robotframework.org/robotframework/latest/libraries/OperatingSystem.html), [Process](https://robotframework.org/robotframework/latest/libraries/Process.html), [Remote](https://robotframework.org/robotframework/latest/libraries/Remote.html), [Telnet](https://robotframework.org/robotframework/latest/libraries/Telnet.html)) and other test tools ([Rebot](https://robotframework.org/robotframework/latest/libraries/Rebot.html), [Libdoc](https://robotframework.org/robotframework/latest/libraries/Libdoc.html), [Testdoc](https://robotframework.org/robotframework/latest/libraries/Testdoc.html), [Tidy](https://robotframework.org/robotframework/latest/libraries/Tidy.html))
    * [API Documentation](https://robot-framework.readthedocs.io/en/stable/) : Use the API to intearct with the Robot Framework Model create your own tools
- [Other libraries on ROBOT FRAMEWORK](https://robotframework.org/#resources), in particular:
    * [AppiumLibrary](http://serhatbolsu.github.io/robotframework-appiumlibrary/AppiumLibrary.html) : Android and iOS testing
    * [RPA framework](https://github.com/robocorp/rpaframework) : Collection of open-source libraries and tools for Robotic Process Automation (RPA), designed to be used both with Robot Framework and Python.
- [Appium](http://appium.io/docs/en/2.0/):
    * [Appium Desired Capabilities](https://appium.readthedocs.io/en/stable/en/writing-running-appium/caps/) : They tell the Appium drivers all kinds of important things about how you want your test to work


## Work on FRIDA trafic interception

### Troubleshooting

### Resources
