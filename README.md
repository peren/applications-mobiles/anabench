ANABENCH
========

API d'automatisation de tests et de réalisation de constats sur applications
mobiles.

## Installation

### Installation de l'API

```bash
python3 -m venv .venv
./.venv/bin/pip install poetry
./.venv/bin/poetry install
npm i
```

Vous devez avoir le SDK Android installé et la variable `$ANDROID_HOME`
remplie sur votre système pour utiliser ce service.

## Configuration

La configuration passe par des fichiers `.env` chargés par
[`dotenv`/`pydantic`](https://docs.pydantic.dev/usage/settings/#dotenv-env-support).

Un modèle de fichier de configuration est fourni dans le dépôt, dans le
fichier `.example.env`. Pour l'utiliser et le personnaliser :

```bash
cp .example.env .env
$EDITOR .env
```

Les réglages par défaut sont adaptés à un poste "développeur". Les valeurs des
réglages peuvent être surchargés temporairement en utilisant des variables
d'environnement portant le même nom.


## Utilisation

### Lancement en local

```bash
./.venv/bin/python -m anabench serve
```
Et ouvrir un navigateur sur [http://127.0.0.1:9026/docs](http://127.0.0.1:9026/docs).

Quelques arguments dans l'environnement:
- `host`, `port` : host et port de l'API Anabench
- `adb_port` : port du service ADB associé
- `appium_port` : port du service Appium associé

```bash
appium_port=17500 port=15600 ./.venv/bin/python -m anabench serve
```


### Lancement sur une machine distante

Par défaut, le service est lancé en écoute sur la machine locale uniquement.
Il est possible de le lancer sur une machine et d'y accéder à distance. Pour
cela, il vous faut dans la configuration (fichier `.env`) :

```
host=0.0.0.0
```

Vous pouvez alors lancer l'API avec

```bash
./.venv/bin/python -m anabench serve
```

et ouvrir un navigateur sur une autre machine à l'adresse
`http://${REMOTE_IP}:9026/docs` où `${REMOTE_IP}` est l'adresse IP de la
machine qui héberge l'API.

Vous pouvez dès lors également vous connecter à la session `adb` lancée par
l'API depuis la machine distante en définissant la variable d'environnement
`ADB_SERVER_SOCKET=tcp:${REMOTE_IP}:5037`.

Par exemple :

```bash
ADB_SERVER_SOCKET=tcp:${REMOTE_IP}:5037 adb devices
```

Sous réserve de définir cette variable d'environnement, vous pouvez alors
lancer localement des utilitaires Android comme
[`scrcpy`](https://github.com/Genymobile/scrcpy#tunnels) ou
[`uiautomatorviewer`](https://developer.android.com/training/testing/other-components/ui-automator)
en utilisant le serveur `adb` lancé par l'API à distance.

## SCRCPY

Les endpoints `start_record` et `stop_record` utilisent la librairie [scrcpy](https://github.com/Genymobile/scrcpy).

Hors PEReN:

```bash
git clone https://github.com/Genymobile/scrcpy
cd scrcpy
./install_release.sh
```

Au PEReN:

```bash
git clone https://github.com/Genymobile/scrcpy
cd scrcpy
# Remove install step from install_release.sh
sed -i 's|^meson setup|meson setup --prefix="$HOME/.local/"|' install_release.sh
sed -i 's|^sudo ||' install_release.sh
# Build scrcpy from source
./install_release.sh
```

## Divers

**TODO** Pourquoi ?

A chaque redémarrage, il est nécessaire de redéfinir la valeur du paramètre
`setenforce` sur le téléphone :

```bash
adb shell setenforce 0
```


## Liens utiles

* [PiRogue](https://pts-project.org/docs/pirogue/build-a-pirogue/)


## Licence

Ce code est publié sous une licence MIT.
