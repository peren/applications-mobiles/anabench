import datetime
import glob
import json
import lzma
import os
import subprocess
import tempfile
import zipfile
from lxml import etree
from zipfile import ZipFile

import frida
import numpy as np
import pandas as pd
import plotnine as p9


def parse_http2(http2, request):
    headers_method = http2.get("http2_http2_headers_method")
    if headers_method:
        header_names = http2["http2_http2_header_name"]
        header_values = http2["http2_http2_header_value"]
        res = {}
        res["timestamp"] = request["timestamp"]
        res["method"] = header_values[0]
        res["host"] = http2["http2_http2_headers_authority"]
        res["path"] = http2["http2_http2_headers_path"]
        headers = {header_names[i]: header_values[i] for i in range(len(header_names)) if header_names[i][0] != ":"}
        res["headers"] = headers
        data = http2.get("http2_http2_data_data")
        if data:
            try:
                hexa_list = data.split(":")
                int_list = [int(hexa_str, 16) for hexa_str in hexa_list]
                data_json = json.loads(bytes(int_list))
                res["data"] = data_json
            except Exception:
                return None
        return res
    return None


def parse_http1(http, request):
    headers_method = http.get("http_http_request_method")
    if headers_method:
        res = {}
        res["timestamp"] = request["timestamp"]
        res["method"] = headers_method
        request_lines = http["http_http_request_line"]
        headers = {
            request_line.split(":")[0].lower(): request_line.split(":")[1][:-2] for request_line in request_lines
        }
        res["host"] = headers.pop("host")
        res["headers"] = headers
        res["path"] = http["http_http_request_uri"]
        data = http.get("http_http_file_data")
        if data:
            try:
                data_json = json.loads(data)
                res["data"] = data_json
            except Exception:
                res["data"] = data
        return res
    return None


def parse_http_requests(interception_filename="./output/traffic.json"):
    with open(interception_filename) as json_file:
        requests = []
        while True:
            line = json_file.readline()
            if not line:
                break
            request = json.loads(line)
            layers = request.get("layers")
            if layers:
                http2 = layers.get("http2")
                if http2:
                    if type(http2) != list:
                        res = parse_http2(http2, request)
                        if res:
                            requests.append(res)
                http = layers.get("http")
                if http:
                    res = parse_http1(http, request)
                    if res:
                        requests.append(res)
    return requests


def add_test_phases(requests):
    test_times = []
    test_names = []
    tree = etree.parse("./output.xml")
    for kw in tree.xpath("/robot/suite/test"):
        test_name = kw.get("name") + " "
        test_names.append(test_name)
        for arg in kw.xpath("arg"):
            if "${" in arg.text:
                test_name += " " + arg.text
        for status in kw.xpath("status"):
            starttime = datetime.datetime.strptime(status.get("starttime") + "000", "%Y%m%d %H:%M:%S.%f")
            endtime = datetime.datetime.strptime(status.get("endtime") + "000", "%Y%m%d %H:%M:%S.%f")
        test_times.append([test_name, starttime, endtime])
    for request in requests:
        for test_time in test_times:
            if (
                datetime.datetime.fromtimestamp(int(request["timestamp"][:-3])) >= test_time[1]
                and datetime.datetime.fromtimestamp(int(request["timestamp"][:-3])) < test_time[2]
            ):
                request["test"] = test_time[0]
    return requests, test_names


def get_domains_graph(data, rules):
    list_domains = []
    for rule in rules.keys():
        domains = []
        for request in data[rule]:
            if type(request["host"]) == list:
                domains.append(request["host"][0])
            else:
                domains.append(request["host"])
        list_domains.append(domains)
    y_axis = list(set([domain for domains in list_domains for domain in domains]))
    data = np.full((len(y_axis), len(list_domains)), False)

    for i in range(0, len(y_axis)):
        for j in range(0, len(list_domains)):
            if y_axis[i] in list_domains[j]:
                data[i][j] = True

    df = pd.DataFrame(data, index=y_axis, columns=rules.keys())
    df = pd.melt(df, ignore_index=False).reset_index()
    df.columns = ["Domaine", "Donnée", "Donnée partagée"]
    df["Donnée"] = pd.Categorical(df.Donnée, categories=pd.unique(df.Donnée))
    domaine_categorical = pd.Categorical(df.Domaine, categories=pd.unique(df.Domaine))

    fig = (
        p9.ggplot(df, p9.aes("Donnée", y=domaine_categorical, fill="Donnée partagée"))
        + p9.geom_tile(p9.aes(width=0.95, height=0.95))
        + p9.scale_y_discrete(labels=lambda y: list(map(lambda x: x if len(x) < 28 else x[:25] + "...", y)))
        + p9.theme(  # new
            axis_ticks=p9.element_blank(),
            panel_background=p9.element_rect(fill="white"),
            axis_text_x=p9.element_text(angle=30),
        )
        + p9.labs(y="Domaine")
    )
    fig.save("./output/domains.png", dpi=300)
    return None


def get_phases_graph(data, rules, test_phases):
    list_phases = []
    for rule in rules.keys():
        phases = []
        for request in data[rule]:
            if request.get("test"):
                phases.append(request["test"])
        list_phases.append(phases)
    y_axis = list(set([phase for phases in list_phases for phase in phases]))
    for test_phase in test_phases:
        if test_phase not in y_axis:
            y_axis.append(test_phase)
    data = np.full((len(y_axis), len(list_phases)), False)

    for i in range(0, len(y_axis)):
        for j in range(0, len(list_phases)):
            if y_axis[i] in list_phases[j]:
                data[i][j] = True

    df = pd.DataFrame(data, index=y_axis, columns=rules.keys())
    df = pd.melt(df, ignore_index=False).reset_index()
    df["index"] = df["index"].astype("category")
    df["index"].cat.set_categories(test_phases, inplace=True)
    df.columns = ["Phase du test", "Donnée", "Donnée partagée"]

    fig = (
        p9.ggplot(df, p9.aes("Phase du test", "Donnée", fill="Donnée partagée"))
        + p9.geom_tile(p9.aes(width=0.95, height=0.95))
        + p9.theme(  # new
            axis_ticks=p9.element_blank(),
            panel_background=p9.element_rect(fill="white"),
            axis_text_x=p9.element_text(angle=30),
        )
    )
    fig.save("./output/phases.png", dpi=300)
    return None


def zipfiles(filenames):
    zip = ZipFile("./output/summary.zip", mode="w", compression=zipfile.ZIP_DEFLATED)
    for filename in filenames:
        zip.write(filename)
    zip.close()
    return zip


def get_test_time(data):
    tree = etree.parse("./output/output.xml")
    new_requests = []
    for request in data["all"]:
        timestamp_string = tree.xpath("/robot/suite/test/kw")[1].xpath("status")[0].get("endtime")
        timestamp_int = datetime.datetime.timestamp(
            datetime.datetime.strptime(timestamp_string + "000", "%Y%m%d %H:%M:%S.%f")
        )
        request_timestamp = int(request["timestamp"]) / 1000
        request["video_time"] = max(request_timestamp - timestamp_int, 0)
        new_requests.append(request)
    data["all"] = new_requests
    return data
