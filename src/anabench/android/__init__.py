import io
import os
import re
import subprocess
import tempfile
import zipfile

from ..utils import _run_command


def clear_app_data(package, uid=None):
    """
    Clear application user data + cache for the specified Android package.
    """
    try:
        process = _run_command((["adb", "shell", "pm", "clear", package] + (["--user", uid] if uid else [])))
        process.check_returncode()
        return process
    except subprocess.CalledProcessError:
        return None


def force_app_stop(package, uid=None):
    """
    Force exit of the specified Android package.
    """
    try:
        process = _run_command((["adb", "shell", "am", "force-stop", package] + (["--user", uid] if uid else [])))
        process.check_returncode()
        return process
    except subprocess.CalledProcessError:
        return None


def download_apk(package, uid=None):
    """
    Download APKs from a given application identifier.

    :param package: Application identifier.
    :param uid: User id of the user owning the application.
    :return: A zip of the various APKs (base + split) as a byte string.
    """
    try:
        adb_process = _run_command(
            (["adb", "shell", "pm", "path", package] + (["--user", uid] if uid else [])),
            stderr=subprocess.DEVNULL,
            stdout=subprocess.PIPE,
        )
        adb_process.check_returncode()
        apk_paths = adb_process.stdout.decode("utf-8").split()
    except subprocess.CalledProcessError:
        raise Exception("Application not found/installed!")

    with tempfile.TemporaryDirectory() as tmpdirname:
        for path in apk_paths:
            path = re.sub("^package:", "", path)
            try:
                process = _run_command(
                    ["adb", "pull", path, tmpdirname], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL
                )
                process.check_returncode()
            except subprocess.CalledProcessError:
                raise Exception("Cannot retrieve apk from device")

        zip_buffer = io.BytesIO()
        with zipfile.ZipFile(zip_buffer, mode="a", compression=zipfile.ZIP_DEFLATED) as zip_file:
            for filename in os.listdir(tmpdirname):
                with open(os.path.join(tmpdirname, filename), "rb") as fh:
                    zip_file.writestr(filename, fh.read())
        return zip_buffer.getvalue()


def get_android_cpu_architecture():
    """
    Get Android CPU architecture from ADB.

    .. note ::
        Adapted from pirogue-cli.
    """
    cpu_process = _run_command(
        ["adb", "shell", "getprop", "ro.product.cpu.abi"],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    cpu_process.check_returncode()
    cpu = cpu_process.stdout.decode()
    if "arm64" in cpu:
        return "arm64"
    if "x86_64" in cpu:
        return "x86_64"
    if "arm" in cpu:
        return "arm"
    if "x86" in cpu:
        return "x86"
    return cpu


def dumpsys(package=None, skip=None):
    """
    Run Android dumpsys command on the whole system or on a given application
    package.

    :param package: Optional package identifier.
    :param skip: Optional list of services to skip.
    """
    maybe_skip_services = []
    if skip:
        maybe_skip_services = [f"--skip {service}" for service in skip]

    maybe_package = []
    if package:
        maybe_package = ["package", package]

    dumpsys_process = _run_command(
        # Beware, order of package and skip matters here!
        (["adb", "shell", "dumpsys"] + maybe_package + maybe_skip_services),
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    dumpsys_process.check_returncode()
    return dumpsys_process.stdout.decode()


def list_devices():
    """
    List connected devices
    """
    devices_string = _run_command(["adb", "devices", "-l"], stdout=subprocess.PIPE)
    devices_string.check_returncode()
    devices_string = devices_string.stdout.decode()
    return parse_devices(devices_string)


def parse_devices(devices_string):
    """
    Parse adb devices command output and returns a list of devices.
    """
    devices = []
    model_pattern = re.compile(r"model\:[\w\d]*\s")
    serial_pattern = re.compile(r"^[\d\w]*")
    for line in devices_string.split("\n")[1:]:
        if line:
            model = model_pattern.search(line).group().strip()
            serial = serial_pattern.search(line).group()
            devices.append({"model": model.strip("model:"), "serial": serial})
    return devices


def retrieve_launcher_package():
    """
    Retrieves phone's homepage information in order to get packageName.

    See https://android.stackexchange.com/questions/227155/retrieve-list-of-default-apps-via-adb/227167#227167
    """
    home_info = _run_command(
        [
            "adb",
            "shell",
            "cmd",
            "package",
            "resolve-activity",
            "-c",
            "android.intent.category.HOME",
            "-a",
            "android.intent.action.MAIN",
        ],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    ).stdout.decode()
    for line in home_info.splitlines():
        line = line.strip()
        if line.startswith("packageName="):
            package_name = re.sub(r"^packageName=", "", line)
            return package_name
    return None
