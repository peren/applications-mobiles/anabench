import pathlib
from subprocess import Popen

from pydantic import BaseSettings, Field
from typing import Optional, Dict


SCRIPT_DIR = pathlib.Path(__file__).parent.resolve()
GIT_REPO_DIR = SCRIPT_DIR.parent.parent


class Config(BaseSettings):
    appium_dir: str = Field(
        str(GIT_REPO_DIR / "node_modules" / ".bin" / "appium"), description="Dir of appium library."
    )
    frida_snippets_dir: str = Field(
        str(GIT_REPO_DIR / "frida-snippets"), description="Path to the Frida snippets directory."
    )
    automations_dir: str = Field(
        str(GIT_REPO_DIR / "robotframework-modules"), description="Paths to the automations (RobotFramework) directory."
    )
    recordings_dir: Optional[pathlib.Path] = Field(
        GIT_REPO_DIR / "recordings", description="Save directory for recordings."
    )
    adb_process: Popen = None
    appium_process: Popen = None
    recording_processes: Dict = dict()

    host: str = Field("127.0.0.1", description="Host for the API to listen on.")
    port: int = Field(9026, description="Port for the API to listen on.")
    adb_port: int = Field(5037, description="Port for the ADB server to listen on.")
    appium_port: int = Field(4723, description="Port for the Appium server to talk with.")

    verbose: str = Field(False, description="Whether the API runs in verbose mode or not.")
    systemd_logging: str = Field(False, description="Whether to log to systemd handler.")

    class Config:
        env_file = ".env"
        schema_extra = {"description": "Handle the environment variable for the app configuration"}


env_config = Config()

# Make sure the directory exists
env_config.recordings_dir.mkdir(parents=True, exist_ok=True)
