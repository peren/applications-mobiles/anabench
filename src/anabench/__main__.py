import argparse
import logging
from typing import Dict, Optional

import uvicorn
from systemd import journal

from . import app
from .config import env_config


class UnknownCommandError(Exception):
    pass


def make_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(prog="ANABENCH", description="Banc de test mobile")
    parser.add_argument("--verbose", default=env_config.verbose, action="store_true")

    subparsers = parser.add_subparsers(dest="cmd")
    subparsers.add_parser("serve", help="Run the built-in API server")
    return parser


def run_with_args(args: argparse.Namespace) -> Optional[Dict]:
    if args.cmd == "serve":
        uvicorn.run(app, host=env_config.host, port=env_config.port)
    else:
        raise UnknownCommandError(f"Unknown command: {args.cmd}")


if __name__ == "__main__":
    # remove the default handler, if already initialized
    existing_handlers = logging.getLogger().handlers
    for handlers in existing_handlers:
        logging.getLogger().removeHandler(handlers)
    # Sending logs to systemd-journal if run via systemd, printing out on console otherwise.
    logging_handler = journal.JournalHandler() if env_config.systemd_logging else logging.StreamHandler()
    logging.getLogger().addHandler(logging_handler)

    parser = make_parser()
    args = parser.parse_args()
    try:
        run_with_args(args)
    except UnknownCommandError:
        parser.print_help()
