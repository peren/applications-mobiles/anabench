from typing import Union

from pydantic import BaseModel, Field


class End2EndQuery(BaseModel):
    """
    Payload model for an end to end request.
    """

    module: Union[str, None] = Field(default="", title="Name of the robot module to run.")
    rules: Union[dict, None] = Field(default={}, title="Set of matching rules.")
