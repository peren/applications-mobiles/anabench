import io
import json
import os
import subprocess
import tempfile

from collections import Counter
from typing import List, Optional

from fastapi import APIRouter, File, Query, Request, UploadFile, HTTPException
from fastapi.responses import FileResponse, PlainTextResponse, RedirectResponse, StreamingResponse

from .analysis import add_test_phases, parse_http_requests, get_domains_graph, get_phases_graph, zipfiles
from .android import clear_app_data, download_apk, dumpsys, force_app_stop, list_devices
from .automations import get_automations_list, run_automation
from .config import env_config
from .frida import get_frida_snippets_list, frida_trace, inject_frida_snippet
from .models import End2EndQuery
from .scrcpy import record_start, record_stop
from .utils import Path

router = APIRouter()


@router.get("/", include_in_schema=False)
async def redirect_to_docs():
    """
    Redirect to /docs page.
    """
    return RedirectResponse("./docs")


@router.get("/automations", summary="List all available automation modules.")
async def list_automations():
    """
    List all available automation modules (Robot Framework modules)
    """
    automations = {}
    for automation in get_automations_list(env_config.automations_dir):
        with open(os.path.join(env_config.automations_dir, automation)) as fh:
            automations[automation] = fh.read()
    return automations


@router.post(
    "/automation/raw/{package}",
    summary="Run a raw automation on a given application package.",
    openapi_extra={
        "requestBody": {
            "content": {
                "text/plain": {
                    "schema": {
                        "type": "string",
                    },
                    "example": """\
*** Settings ***
Library     AppiumLibrary

*** Variables ***
${CONSENT}  id=fr.leboncoin:id/agree

*** Test Cases ***
Open_Application
    Open Application  http://localhost:%s/wd/hub platformName=Android automationName=UiAutomator2  autoLaunch=true
Consent
    Wait Until Page Contains Element    ${CONSENT}
    Click Element   ${CONSENT}
Close Application
    Close Application"""
                    % str(env_config.appium_port),
                }
            }
        }
    },
)
async def raw_automation(
    request: Request,
    package: str = Path(default="com.example", description="Application package identifier"),
    force_reset: bool = Query(False, description="Force a restart of the app before running automation."),
    force_clear: bool = Query(False, description="Force cleaning the app cache before running automation."),
):
    robot_module = await request.body()
    with tempfile.NamedTemporaryFile() as tmpfilename:
        tmpfilename.write(robot_module)
        tmpfilename.flush()
        return run_automation(tmpfilename.name, package, force_clear, force_reset)


@router.post(
    "/automation/{package}/{module}",
    summary="Run a pre-existing automation on a given application package.",
)
async def automation(
    package: str = Path(default="com.example", description="Application package identifier"),
    module: str = Path(
        default="default",
        enum=get_automations_list(env_config.automations_dir),
        description="Name of a pre-existing automation",
    ),
    force_reset: bool = Query(False, description="Force a restart of the app before running automation."),
    force_clear: bool = Query(False, description="Force cleaning the app cache before running automation."),
):
    return run_automation(os.path.join(env_config.automations_dir, module), package, force_clear, force_reset)


@router.get("/fridas", summary="List all available Frida snippets.")
async def list_frida_snippets():
    """
    List all available Frida snippets
    """
    frida_snippets = {}
    for snippet in get_frida_snippets_list(env_config.frida_snippets_dir):
        with open(os.path.join(env_config.frida_snippets_dir, f"{snippet}.js")) as fh:
            frida_snippets[snippet] = fh.read()
    return frida_snippets


@router.post(
    "/frida/raw/{package}",
    summary="Inject a raw Frida script into an application",
    openapi_extra={
        "requestBody": {
            "content": {
                "text/plain": {
                    "schema": {
                        "type": "string",
                    },
                    "example": """\
const simulated_latitude  = 48.8534;
const simulated_longitude = 2.3488;

Java.perform(function(){
    Java.use('android.location.Location').getLatitude.implementation = function () {
        console.log("Old latitude : " + this.getLatitude() + ", New Latitude: " + simulated_latitude)
        return simulated_latitude
    }

    Java.use('android.location.Location').getLongitude.implementation = function () {
        console.log("Old longitude : " + this.getLongitude() + ", New Longitude: " + simulated_longitude)
        return simulated_longitude
    }

    Java.use('android.location.Location').isFromMockProvider.implementation = function () {
        console.log("Location.isFromMockProvider -> false")
        return false
    }
})""",
                }
            }
        }
    },
)
async def raw_frida(
    request: Request,
    package: str = Path(default="com.example", description="Application package identifier"),
    force_reset: bool = Query(False, description="Force a restart of the app before running Frida."),
    force_clear: bool = Query(False, description="Force cleaning the app cache before running Frida."),
    user_id: Optional[int] = Query(None, description="Android user ID owning the app, see `adb shell pm list users`."),
    eternalize: bool = Query(False, description=("Detach Frida (eternalize mode)." "Defaults to False.")),
    timeout: int = Query(2, description=("Time required to let the app spawn correctly. " "Defaults to 2 seconds")),
):
    frida_script = await request.body()
    with tempfile.NamedTemporaryFile() as tmpfilename:
        tmpfilename.write(frida_script)
        tmpfilename.flush()
        return inject_frida_snippet([tmpfilename.name], package, force_clear, force_reset, user_id, eternalize, timeout)


@router.post(
    "/frida/{package}",
    summary="Load a pre-existing Frida snippet onto a given application package.",
)
async def frida(
    snippets: List[str] = Query(
        default=["log_ssl_keys", "mask_root"],
        enum=get_frida_snippets_list(env_config.frida_snippets_dir),
        description="Frida snippet to load",
    ),
    package: str = Path(default="com.example", description="Application package identifier"),
    force_reset: bool = Query(False, description="Force a restart of the app before running Frida."),
    force_clear: bool = Query(False, description="Force cleaning the app cache before running Frida."),
    user_id: Optional[int] = Query(None, description="Android user ID owning the app, see `adb shell pm list users`."),
    eternalize: bool = Query(False, description=("Detach Frida (eternalize mode)." "Defaults to False.")),
    timeout: int = Query(2, description=("Time required to let the app spawn correctly. " "Defaults to 2 seconds")),
):
    return inject_frida_snippet(
        [os.path.join(env_config.frida_snippets_dir, f"{snippet}.js") for snippet in snippets],
        package,
        force_clear,
        force_reset,
        user_id,
        eternalize,
        timeout,
    )


@router.post(
    "/frida-trace/{package}",
    summary="Run Frida tracing onto an application.",
    description=(
        "See https://frida.re/docs/frida-trace/ for more details. "
        "Force-stop the app in Android to end trace collection."
    ),
)
async def post_frida_trace(
    request: Request,
    package: str = Path(default="com.example", description="Application package identifier"),
    force_reset: bool = Query(False, description="Force a restart of the app before running Frida."),
    force_clear: bool = Query(False, description="Force cleaning the app cache before running Frida."),
    user_id: Optional[int] = Query(None, description="Android user ID owning the app, see `adb shell pm list users`."),
    include_java_methods: Optional[List[str]] = Query(
        None,
        description=(
            "Java methods to trace, in the form `class!method`. "
            "`*` can be used for glob-matching. "
            "Example: `com.example.*!*`"
        ),
    ),
    include_native: Optional[List[str]] = Query(
        None,
        description=(
            "Native functions to trace, in the form `library!function`. "
            "`*` can be used for glob-matching. "
            "Example: `libssl.so!SSL_CTX_new`"
        ),
    ),
):
    return frida_trace(package, force_clear, force_reset, user_id, java=include_java_methods, native=include_native)


@router.post(
    "/intercept/start/{package}",
    summary="Start network interception onto a given application",
)
async def start_intercept(
    package: str = Path(default="com.example", description="Application package identifier"),
    force_reset: bool = Query(False, description="Force a restart of the app before running Frida."),
    force_clear: bool = Query(False, description="Force cleaning the app cache before running Frida."),
):
    if force_clear:
        clear_app_data(package)
    if force_reset:
        force_app_stop(package)

    with tempfile.TemporaryDirectory() as tmpdirname:
        # TODO: What to return?
        subprocess.Popen(
            [
                "pirogue-intercept-tls",
                "-U",
                "-f",
                package,
                "-o",
                tmpdirname,
            ],
        )


@router.post(
    "/intercept/stop",
    summary="Stop network interception",
)
async def stop_intercept():
    # TODO
    pass


@router.post("/record/start/", summary="Start a video screen record")
async def post_record_start(
    serial_number: Optional[str] = Path(default=None, description="Smartphone serial number to start recording on")
):
    try:
        return f"Your record process identifier is: {record_start(serial_number)}"
    except Exception:
        raise HTTPException(status_code=404, detail="Failed to start recording")


@router.post("/record/stop", summary="Stop a video screen record")
async def post_record_stop(
    public_uuid: str = Query(description="UUID public du process associé au moment du lancement du record"),
):
    other_exception = HTTPException(status_code=404, detail="Could not stop recording")

    try:
        temp_file = record_stop(public_uuid)
    except KeyError as e:
        if e.args[0].startswith("No recording associated to public"):
            raise HTTPException(status_code=404, detail=e.args[0])
        else:
            raise other_exception
    except Exception as e:
        if e.args[0] in ["Process already terminated"]:
            raise HTTPException(status_code=404, detail=e.args[0])
        else:
            raise other_exception

    response = FileResponse(
        str(temp_file),
        media_type="application/x-zip-compressed",
        headers={"Content-Disposition": "attachment;filename=video.zip"},
    )
    return response


@router.post(
    "/apk/{package}",
    summary="Download APKs from apps installed on the smartphone",
)
async def download_apk_route(
    package: str = Path(default="com.example", description="Application package identifier"),
    user_id: Optional[int] = Query(None, description="Android user ID owning the app, see `adb shell pm list users`."),
):
    try:
        return StreamingResponse(
            io.BytesIO(download_apk(package, user_id)),
            media_type="application/x-zip-compressed",
            headers={
                "Content-Disposition": f"attachment;filename={package}.zip",
            },
        )
    except Exception:
        raise HTTPException(status_code=404, detail="Application not found")


@router.post("/analysis/{package}", summary="Given a previous network interception dump, re-run the analysis")
async def analysis(
    payload: End2EndQuery,
    package: str = Path(default="com.example", description="Application package identifier"),
    traffic_json: UploadFile = File(description="traffic.json file obtained from network interception."),
):
    # Parse intercepted HTTP requests
    requests = parse_http_requests(traffic_json.filename)
    if payload.module:
        requests, test_phases = add_test_phases(requests)

    # Analysis: rules match
    rules_match = {}
    for rule_name, rule in payload.rules.items():
        match = []
        for term in rule:
            for request in requests:
                if term in json.dumps(request):
                    match.append(request)
        rules_match[rule_name] = match

    # Analysis: domains
    domains = []
    for request in requests:
        if type(request["host"]) is list:
            domains.append(request["host"][0])
        else:
            domains.append(request["host"])
    domains = dict(Counter(domains))

    # Generate report and graphs
    data = {
        "all": requests,
        "domains": domains,
        "test_summary": {
            "nb_requests": len(requests),
            "nb_domains": len(domains),
        },
    } | rules_match
    get_domains_graph(data, payload.rules)
    if payload.module:
        get_phases_graph(data, payload.rules, test_phases)

    # Build analysis ZIP file
    with tempfile.TemporaryDirectory() as tmpdirname:
        with open(f"{tmpdirname}/json_data.json", "w") as outfile:
            json.dump(data, outfile)
        zipfiles(
            [f"{tmpdirname}/json_data.json", f"{tmpdirname}/domains.png"]
            + ([f"{tmpdirname}/phases.png"] if payload.module else [])
        )
        return FileResponse(
            f"{tmpdirname}/summary.zip",
            media_type="application/x-zip-compressed",
            headers={
                "Content-Disposition": "attachment;filename=summary.zip",
            },
        )


@router.get("/dumpsys", summary="Dump system state through dumpsys android tool", response_class=PlainTextResponse)
async def dumpsys_handler(
    package: str = Query(default=None, description="Application package identifier"),
    skip: list = Query(
        ["meminfo", "window"],
        description=(
            "Dumpsys services to skip, see " "https://developer.android.com/studio/command-line/dumpsys?hl=fr"
        ),
    ),
):
    return dumpsys(package, skip)


@router.get("/devices", summary="List devices connected and get their serial")
async def get_list_devices():
    return list_devices()
