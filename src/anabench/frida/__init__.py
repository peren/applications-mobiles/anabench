import lzma
import os
import requests
import subprocess
import sys
import tempfile
import time

import frida

from ..android import clear_app_data, force_app_stop, get_android_cpu_architecture
from ..utils import list_files, _run_command


def download_frida_server(arch: str, output_file, platform: str, client_version: str):
    """
    Download the matching Frida server to the specified location.

    :param aarch: CPU architecture.
    :param output_file: Path to output the frida-server binary.
    :param platform: Frida platform (android, etc.).
    :param client_version: Frida-server version to download.

    .. note ::
        Adapted from pirogue-cli.
    """
    FRIDA_SERVER_RELEASES_URL = "https://api.github.com/repos/frida/frida/releases"

    releases = requests.get(FRIDA_SERVER_RELEASES_URL).json()
    for release in releases:
        tag_name = release.get("tag_name")
        if tag_name == client_version:
            for asset in release.get("assets"):
                asset_name = asset.get("name")
                if "server" in asset_name and f"{platform}-{arch}.xz" in asset_name:
                    xz_file = requests.get(asset["browser_download_url"])
                    server_binary = lzma.decompress(xz_file.content)
                    with open(output_file, mode="wb") as out:
                        out.write(server_binary)
                        out.flush()
                    return


def get_frida_snippets_list(frida_snippets_dir):
    return [os.path.splitext(item)[0] for item in list_files(str(frida_snippets_dir), "js")]


def is_frida_server_running():
    """
    Check whether the Frida server is running on the smartphone.

    .. note ::
        Adapted from pirogue-cli.
    """
    try:
        adb_process = _run_command(
            ["adb", "shell", "ps", "-A"],
            stdout=subprocess.PIPE,
            stderr=subprocess.DEVNULL,
        )
        adb_process.check_returncode()
        return "frida-server" in adb_process.stdout.decode()
    except subprocess.CalledProcessError:
        return False


def ensure_frida_is_running(spawn_timeout=10):
    """
    Ensure the correct Frida version is running.

    .. note ::
        Adapted from pirogue-cli.
    """
    if is_frida_server_running():
        # TODO: Check this is the correct version which is running
        return

    with tempfile.NamedTemporaryFile() as tmpfilename:
        aarch = get_android_cpu_architecture()
        frida_server_path = f"/data/local/tmp/frida-server-{frida.__version__}-android-{aarch}"

        has_frida_server_downloaded = True
        try:
            adb_process = _run_command(
                ["adb", "shell", "ls", frida_server_path], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL
            )
            adb_process.check_returncode()
        except subprocess.CalledProcessError:
            has_frida_server_downloaded = False

        if not has_frida_server_downloaded:
            # Download Frida-server
            download_frida_server(aarch, tmpfilename.name, "android", frida.__version__)
            tmpfilename.flush()
            # Install it onto the smartphone
            _run_command(
                ["adb", "push", tmpfilename.name, frida_server_path],
                stdout=subprocess.DEVNULL,
                stderr=subprocess.DEVNULL,
            ).check_returncode()
            _run_command(
                ["adb", "shell", "chmod", "+x", frida_server_path],
                stdout=subprocess.DEVNULL,
                stderr=subprocess.DEVNULL,
            ).check_returncode()

        # Run frida-server in the background
        subprocess.Popen(["adb", "shell", f'su -c "{frida_server_path} &"'])

        # Previous call is non-blocking
        # Frida-server will not start before user has confirmed su access
        # Threfore, wait for frida-server to effectively spawn
        start_time = time.time()
        while True:
            try:
                # Is frida-server running?
                _run_command(
                    ["adb", "shell", "ps -A | grep frida-server"], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL
                ).check_returncode()
                # Yes => return
                return
            except subprocess.CalledProcessError:
                # No => Check for timeout and wait
                if time.time() - start_time > spawn_timeout:
                    raise
                time.sleep(1)


def inject_frida_snippet(
    snippet_files, package, force_clear=False, force_reset=True, user_id=None, eternalize=False, timeout=2
):
    """
    Inject frida snippets into a given application

    :param snippet_files: List of snippet files to inject.
    :param package: Android package identifier (``com.example.app``).
    :param force_clear: Whether to clear application cache prior to execution.
        Defaults to ``False``.
    :param force_reset: Whether to force a reset of the app prior to execution.
        Defaults to ``True``.
    :param user_id: Android user identifier (multi-user devices).
        Defaults to ``None``.
    :param eternalize: Whether to eternalize the script and detach from Frida
        process. Defaults to ``False``.
    :param timeout: Time for Frida to wait for script execution.
        Defaults to 2s.
    """
    ensure_frida_is_running()

    if force_clear:
        clear_app_data(package, user_id)

    if force_reset:
        force_app_stop(package, user_id)

    frida_process = None
    had_errors, stdout, stderr = False, "", ""
    try:
        extra_spawn_args = [f"--aux=uid=(int){user_id}"] if user_id else []
        # -f argument should be immediately before package name
        extra_spawn_args += ["-f"] if force_reset else []

        maybe_eternalize_args = []
        if eternalize:
            maybe_eternalize_args = ["--eternalize", "-q", "-t", str(timeout)]

        snippets_arguments = []
        for snippet_file in snippet_files:
            snippets_arguments.extend(["-l", snippet_file])

        frida_process = _run_command(
            (
                # That is "frida" binary
                [sys.executable, "-m", "frida_tools.repl", "-U"]
                + extra_spawn_args
                + [package]
                + snippets_arguments
                + maybe_eternalize_args
            ),
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            # We want to run in background (detach / non blocking) if not
            # eternalizing the script.
            background=(not eternalize),
        )
        # If not eternalize and we have a timeout, we let some time for the
        # frida command to spawn and run.
        if not eternalize and timeout:
            time.sleep(timeout)
        if frida_process.returncode is not None and frida_process.returncode != 0:
            raise subprocess.CalledProcessError(frida_process.returncode, frida_process.pid)
        frida_process.check_returncode()
    except subprocess.CalledProcessError:
        had_errors = True

    if frida_process is not None:
        if eternalize:
            stdout, stderr = frida_process.stdout.decode(), frida_process.stderr.decode()
        else:
            os.set_blocking(frida_process.stdout.fileno(), False)
            stdout = frida_process.stdout.read()
            os.set_blocking(frida_process.stderr.fileno(), False)
            stderr = frida_process.stderr.read()
            if stdout:
                stdout = stdout.decode()
            if stderr:
                stderr = stderr.decode()
    return {
        "had_errors": had_errors,
        "stdout": stdout,
        "stderr": stderr,
    }


def frida_trace(package, force_clear, force_reset, user_id, java=None, native=None):
    ensure_frida_is_running()

    if force_clear:
        clear_app_data(package, user_id)

    if force_reset:
        force_app_stop(package, user_id)

    frida_process = None
    had_errors, stdout, stderr = False, "", ""
    try:
        extra_spawn_args = [f"--aux=uid=(int){user_id}"] if user_id else []
        # -f argument should be immediately before package name
        extra_spawn_args += ["-f"] if force_reset else []

        java_methods_args = []
        for item in java or []:
            java_methods_args.append("-j")
            java_methods_args.append(item)

        native_args = []
        for item in native or []:
            native_args.append("-i")
            native_args.append(item)

        frida_process = _run_command(
            (
                # That is "frida-trace" binary
                [sys.executable, "-m", "frida_tools.tracer", "-U"]
                + extra_spawn_args
                + [package]
                + java_methods_args
                + native_args
            ),
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        # When force-stopping the app, frida-trace will exit with exit code 1
        # Therefore, we are not checking return code.
        frida_process.check_returncode()
    except subprocess.CalledProcessError:
        had_errors = True

    if frida_process is not None:
        stdout, stderr = frida_process.stdout.decode(), frida_process.stderr.decode()
    return {
        "had_errors": had_errors,
        "stdout": stdout,
        "stderr": stderr,
    }
