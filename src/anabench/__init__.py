import logging

from fastapi import FastAPI

from .routes import router
from .utils import start_servers, stop_servers

logging.basicConfig(level=logging.INFO)

app = FastAPI()
app.include_router(router)


@app.on_event("startup")
async def startup_event():
    """
    Ensure adb server is correctly started and start appium subprocess upon API
    startup.
    """
    start_servers()


@app.on_event("shutdown")
async def shutdown_event():
    """
    Stop appium subprocess upon API shutdown.
    """
    stop_servers()
