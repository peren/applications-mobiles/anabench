import glob
import logging
import os
import subprocess
from typing import Any

from fastapi.params import Path as OrigPath, Undefined
from .config import env_config


class Path(OrigPath):
    """
    Overload fastapi.Path to let user specify a default value.

    See https://github.com/tiangolo/fastapi/issues/5019#issuecomment-1152314791
    """

    def __init__(self, default: Any = Undefined, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if default is not Undefined:
            self.default = default


def list_files(path, extension):
    """
    Recursively list all files in the given path ending with extension.
    """
    return [
        item.replace(path, "").lstrip("/\\")
        for item in glob.glob(os.path.join(path, "**", f"*.{extension}"), recursive=True)
    ]


def _run_command(command, stdout=None, stderr=None, background=False):
    """
    Run and log command, wrapper around subprocess.
    """
    logging.info('Running command "%s"...', " ".join(command))
    if background:
        return subprocess.Popen(
            command,
            stdout=stdout,
            stderr=stderr,
        )

    return subprocess.run(
        command,
        stdout=stdout,
        stderr=stderr,
    )


def start_servers(appium_loglevel: str = "warn", appium_logfile: Path = None):
    """
    Ensure adb server is correctly started and start appium subprocess upon API
    startup.

    :param appium_loglevel: Log level of appium process. Default to "warn".
        If None, arg is not passed
    :param appium_logfile: Path for a file where to save appium logs.
        If None, no logs saved (default).
    """
    logger = logging.getLogger("uvicorn.error")

    # Run Appium
    try:
        maybe_appium_loglevel = ["--log-level", appium_loglevel] if appium_loglevel is not None else []
        maybe_appium_logfile = ["--log", appium_logfile] if appium_logfile is not None else []
        env_config.appium_process = subprocess.Popen(
            [env_config.appium_dir, "-p", str(env_config.appium_port)] + maybe_appium_loglevel + maybe_appium_logfile
        )
        logger.info(f"ADB server will talk to Appium server located at {env_config.host}:{env_config.appium_port}.")
    except FileNotFoundError:
        logger = logging.getLogger("uvicorn.error")
        logger.error("Appium binary could not be found. Have you run `npm i`?")

    # Stop any currently running adb server
    try:
        subprocess.check_output(["adb", "kill-server"], stderr=subprocess.DEVNULL)
    except subprocess.CalledProcessError:
        # adb was not running, ignore
        pass

    # Restart adb server
    try:
        # Listen on all interfaces at port 5037
        adb_listen_on_all_interfaces_args = []
        if env_config.host != "127.0.0.1":
            logger.info("Running ADB on all network interfaces.")
            adb_listen_on_all_interfaces_args = ["-a"]
        env_config.adb_process = subprocess.Popen(
            ["adb"]
            + adb_listen_on_all_interfaces_args
            + ["-P", str(env_config.adb_port), "nodaemon", "server", "start"]
        )
        logger.info(f"ADB server is running at tcp:{env_config.host}:{env_config.adb_port}.")
    except subprocess.CalledProcessError:
        logger.error("Could not start ADB server!")


def stop_servers():
    """
    Stop appium subprocess upon API shutdown.
    """
    logger = logging.getLogger("uvicorn.error")
    try:
        if env_config.adb_process is not None:
            env_config.adb_process.kill()
            logger.info(f"Killed flawlessly : ADB server at tcp:{env_config.host}:{env_config.adb_port}.")
            env_config.adb_process = None
    except AttributeError or subprocess.CalledProcessError:
        # adb was not running, ignore
        pass
    try:
        if env_config.appium_process is not None:
            env_config.appium_process.kill()
            logger.info(f"Killed flawlessly : Appium server located at {env_config.host}:{env_config.appium_port}.")
            env_config.appium_process = None
    except AttributeError or subprocess.CalledProcessError:
        # appium was not running, ignore
        pass
