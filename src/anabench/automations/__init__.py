import os
import subprocess
import sys
import tempfile
import time
from jinja2.sandbox import ImmutableSandboxedEnvironment

from ..android import clear_app_data, force_app_stop, retrieve_launcher_package
from ..utils import list_files, _run_command


def get_automations_list(automations_dir):
    return [item for item in (list_files(str(automations_dir), "robot") + list_files(str(automations_dir), "j2"))]


def run_automation(automation_file, package, force_clear, force_reset):
    """
    Run an automation query
    """
    if force_clear:
        clear_app_data(package)
    if force_reset:
        force_app_stop(package)

    had_errors, robot_stdout, robot_stderr, robot_output = False, None, None, None
    try:
        _run_command(
            ["adb", "shell", "monkey", "-p", package, "1"], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL
        ).check_returncode()
    except subprocess.CalledProcessError:
        return {
            "had_errors": True,
            "error": f"Unable to start application {package}",
        }

    time.sleep(1)  # Let time for the app to launch
    try:
        launcher_package = retrieve_launcher_package()
        sandboxed_environment = ImmutableSandboxedEnvironment()
        with open(automation_file, "r") as fh:
            file_string = fh.read()
        template = sandboxed_environment.from_string(file_string)

        with tempfile.TemporaryDirectory() as tmp_robot_dir:
            with tempfile.NamedTemporaryFile(mode="w") as tmpfilename:
                tmpfilename.write(template.render(launcher_package=launcher_package))
                tmpfilename.flush()
                robot_process = _run_command(
                    [
                        sys.executable,
                        "-m",
                        "robot",
                        "--console",
                        "verbose",
                        "-o",
                        f"{tmp_robot_dir}/output.xml",
                        "-r",
                        "none",  # no report.html
                        "--log",
                        "none",  # no log.html
                        tmpfilename.name,  # tests suite
                    ],
                    stdout=subprocess.PIPE,
                    stderr=subprocess.PIPE,
                )
                robot_process.check_returncode()
                if os.path.isfile(f"{tmp_robot_dir}/output.xml"):
                    with open(f"{tmp_robot_dir}/output.xml", "r") as fh:
                        robot_output = fh.read()
    except subprocess.CalledProcessError:
        had_errors = True

    robot_stdout = robot_process.stdout.decode()
    robot_stderr = robot_process.stderr.decode()

    try:
        _run_command(["adb", "shell", "am", "force-stop", package]).check_returncode()
    except subprocess.CalledProcessError:
        return {
            "had_errors": True,
            "error": f"Unable to stop application {package}",
        }

    return {
        "had_errors": had_errors,
        "robot_stdout": robot_stdout,
        "robot_stderr": robot_stderr,
        "output": robot_output,
    }
