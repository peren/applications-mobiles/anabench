import os
import pathlib
import subprocess
import signal
import tempfile
import time
import uuid
import zipfile

from tempfile import mkstemp
from zipfile import ZipFile

from ..utils import _run_command
from ..config import env_config


def record_start(serial_number=None, max_fps=None):
    """
    Starts a screen recording and returns the process id associated
    If several phones are connected, provide a serial number (refer to /devices).
    """
    # Share a public ID through the API response
    public_uuid = uuid.uuid4().hex
    if public_uuid in env_config.recording_processes:
        # Should not happen with uuid4, see https://docs.python.org/3/library/uuid.html#uuid.uuid4  # noqa E501
        raise KeyError(f"Public uuid {public_uuid} already exists, should be unique.")

    # Start recording
    maybe_serial_number = ["-s", str(serial_number)] if serial_number else []
    maybe_max_fps = [f"--max-fps={max_fps}"] if max_fps is not None else []
    video_file_path = pathlib.Path(env_config.recordings_dir) / f"{public_uuid}.mkv"
    command_args = ["scrcpy", "-Nr", str(video_file_path)] + maybe_serial_number + maybe_max_fps
    record_process = _run_command(command_args, background=True)

    # Map the public uuid to the private recording process id
    env_config.recording_processes[public_uuid] = record_process.pid
    return public_uuid


def record_stop(public_uuid: str, zip_recording=True):
    """
    Ends a screen recording associated with provided process id.
    Returns the path of the recorded video
    """
    # Get id of the recording process initiated by a previous call of record_start function
    if public_uuid not in env_config.recording_processes:
        raise KeyError(f"No recording associated to public uuid {public_uuid}")
    process_id = env_config.recording_processes[public_uuid]
    try:
        os.kill(int(process_id), signal.SIGINT)
        time.sleep(2)  # Give time to load video.mkv
    except Exception:
        raise Exception("Process already terminated")
    del env_config.recording_processes[public_uuid]

    video_file_path = pathlib.Path(env_config.recordings_dir) / f"{public_uuid}.mkv"
    if not zip_recording:
        return video_file_path

    # Save the recording into a zip file
    # Pass to temporary file
    temp_file = tempfile.NamedTemporaryFile(delete=False)
    with zipfile.ZipFile(temp_file, mode="w", compression=zipfile.ZIP_DEFLATED) as my_zip:
        my_zip.write(video_file_path, arcname=video_file_path.name)
    video_file_path.unlink()
    return pathlib.Path(temp_file.name)
